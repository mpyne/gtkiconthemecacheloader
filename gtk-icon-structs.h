#ifndef QT_GTK_ICON_CACHE_VIEWER_STRUCTS_H
#define QT_GTK_ICON_CACHE_VIEWER_STRUCTS_H

#include <QtCore/QtEndian>
#include <QtCore/QVector>

// To be reinterpret_cast<>'ed at the beginning of a memory map
// containing the GTK icon cache file.
struct GtkIconCacheHeader
{
    public:
    qint16 majorVersion() const    { return qFromBigEndian(_majorVersion); }
    qint16 minorVersion() const    { return qFromBigEndian(_minorVersion); }
    qint32 hashTableOffset() const { return qFromBigEndian(_hashTableOffset); }
    qint32 dirListOffset() const   { return qFromBigEndian(_dirListOffset); }
    qint32 hashTableSize() const   { return qFromBigEndian(_hashTableSize); }

    private:
    qint16 _majorVersion;
    qint16 _minorVersion;
    qint32 _hashTableOffset;
    qint32 _dirListOffset;
    qint32 _hashTableSize;
};

struct GtkIconCacheHashTable
{
    public:
    qint32 hashTableSize() const { return qFromBigEndian(_hashTableSize); }

    QVector<qint32> bucketOffsets() const {
        // Vector of BE->local converted offsets.
        qint32 size = hashTableSize();
        QVector<qint32> resultOffsets(size);
        const qint32* bigEndianOffsets = reinterpret_cast<const qint32 *>(
                &(this->_bucketOffsets));

        for (qint32 i = 0; i < size; i++) {
            resultOffsets[i] = qFromBigEndian(bigEndianOffsets[i]);
        }

        return resultOffsets;
    }

    private:
    qint32 _hashTableSize;
    qint32 _bucketOffsets; // Really an array, of size hashTableSize
};

struct GtkIconCacheHashBucket
{
    public:
    qint32 nextNodeOffset()  const { return qFromBigEndian(_nextNodeOffset); }
    qint32 nodeNameOffset()  const { return qFromBigEndian(_nodeNameOffset); }
    qint32 imageListOffset() const { return qFromBigEndian(_imageListOffset); }

    private:
    qint32 _nextNodeOffset;
    qint32 _nodeNameOffset;
    qint32 _imageListOffset;
    char   _nodeName[1]; // Variable-length unfortunately
    // qint32 nodeImageListLength; // This is present, but behind a var-length array.
};

#endif /* QT_GTK_ICON_CACHE_VIEWER_STRUCTS_H */
