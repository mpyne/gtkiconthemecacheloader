# README

This is a Qt-based demo application to read a GTK+ icon theme cache.

## Docs

What I've compiled as far as file format documentation can be [found here](icon-theme-cache-format.md).
