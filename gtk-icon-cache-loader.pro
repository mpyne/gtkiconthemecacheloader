TEMPLATE = app
TARGET = gtk-icon-cache-loader
DEPENDPATH += .
INCLUDEPATH += .
CONFIG += debug
QMAKE_CXXFLAGS += -std=c++0x

# Input
HEADERS += gtk-icon-cache-loader.h
SOURCES += gtk-icon-cache-loader.cpp
FORMS += cacheLoaderMainWindow.ui
