#ifndef GTK_ICON_CACHE_LOADER_H
#define GTK_ICON_CACHE_LOADER_H

#include <QtGui/QWidget>

namespace Ui {
    class CacheLoaderMainWindow;
}

class GtkIconCacheLoaderMainWindow : public QWidget
{
    Q_OBJECT

public:
    GtkIconCacheLoaderMainWindow(QWidget *parent = 0);

private slots:
    void openFileSelectDialog();

private:
    void openFile(const QString &fileName);

private:
    Ui::CacheLoaderMainWindow *m_ui;
};

#endif
