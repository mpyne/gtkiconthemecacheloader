## GTK+ Icon Theme Cache Format

As of version 1.0 of the format

### Header

    int16 majorVersion;
    int16 minorVersion;
    int32 hashTableOffset;
    int32 dirListOffset;

### Hash Table

    int32 hashTableSize; // ?

    int32 bucketOffsets[hashTableSize] // 0xffffffff is null

#### non-null Buckets
    {
        int32 nextNodeOffset; // 0xffffffff is null
        int32 nodeNameOffset; // should not be null, maybe negative?
                              // NOTE: Seems to be based from file start, not bucket start.
                              // This would tend to mean negatives are not possible.
        int32 imageListOffset; // should not be null

        char  nodeName[??]; // Only if node name wasn't used elsewhere, possibly indicated
                            // by nodeNameOffset being equal to 12 (relative to its own pos)
                            // length is alignTo<4>(nodeName.length + 1)
                            // End is pointed to by imageListOffset above.
        int32 nodeImageListLength; // Possibly 0

##### Node Image List
        {
            int16 imageDirectoryIndex;
            int16 imageFlags;
            int32 imageDataOffset; // Can be 0, which is null
        } [nodeImageListLength]

##### non-Null Node Image Data
        {
            uint32 imageDataOffset;    // Redundant??? 0 is null
            int32 imageMetaDataOffset; // 0 is null
            byte  imageData[];         // Only if present (BMS!)
            byte  imageMetaData[];     // Only if present (BMS!)
        } [nodeImageListLength - # null images]
    }

### Directory Index
    int32 directoryListLength;
    {
        int32 offsetToDirEntryName; // Should be positive
    } [directoryListLength]
    char [] stringPool; // Each string is 4-aligned, 0-terminated
    EOF
