#include "gtk-icon-cache-loader.h"

#include <QtDebug>
#include <QtGui/QApplication>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QFileDialog>
#include <QtGui/QMessageBox>
#include <QtGui/QLabel>
#include <QtGui/QPlainTextEdit>
#include <QtGui/QStandardItemModel>
#include <QtGui/QTreeView>
#include <QtCore/QFile>
#include <QtCore/QDataStream>
#include <QtCore/QList>
#include <QtCore/QStringList>

#include "ui_cacheLoaderMainWindow.h"
#include "gtk-icon-structs.h"

static const uchar *offsetOf(
        const uchar *base,
        const uchar *start,
        qint64       size,
        qint32       offset
        )
{
    // Only unsigned integer arithmetic is defined in the face of overflow
    // TODO: Make 64-bit/large file capable?
    quintptr baseUInt  = reinterpret_cast<quintptr>(base);
    quintptr startUInt = reinterpret_cast<quintptr>(start);
    quint32 offsetUInt = static_cast<quint32>(offset);
    quint32 sizeUInt   = static_cast<quint32>(size);

    Q_ASSERT(baseUInt >= startUInt);

    if (
            (baseUInt + offsetUInt) < baseUInt                ||
            (baseUInt + offsetUInt) >= (startUInt + sizeUInt) ||
            (baseUInt + offsetUInt) < startUInt)
    {
        return 0;
    }

    return reinterpret_cast<const uchar *>(baseUInt + offsetUInt);
}

GtkIconCacheLoaderMainWindow::GtkIconCacheLoaderMainWindow(QWidget *parent)
    : QWidget(parent)
    , m_ui(new Ui::CacheLoaderMainWindow)
{
    setObjectName(QLatin1String("GtkIconCacheLoaderMainWindow"));
    m_ui->setupUi(this);

    connect(m_ui->buttonBox, SIGNAL(rejected()), SLOT(close()));
    connect(m_ui->buttonBox, SIGNAL(accepted()), SLOT(openFileSelectDialog()));
}

void GtkIconCacheLoaderMainWindow::openFileSelectDialog()
{
    QString selectedFile = QFileDialog::getOpenFileName(
            this, QString(), QLatin1String("/usr/share/icons"),
            QLatin1String("GTK Icon Caches (*.cache)"));

    if (!selectedFile.isEmpty()) {
        openFile(selectedFile);
    }
}

void GtkIconCacheLoaderMainWindow::openFile(const QString &fileName)
{
    QPlainTextEdit *const textLog = m_ui->textLog;

    textLog->clear();
    textLog->appendPlainText(QString::fromLatin1("Opening %1").arg(fileName));

    QFile cacheFile(fileName);
    if (!cacheFile.open(QFile::ReadOnly)) {
        textLog->appendPlainText(
                QString::fromLatin1("Unable to open file %1").arg(fileName));
        return;
    }

    qint64 cacheSize = cacheFile.size();
    const uchar *cacheData = cacheFile.map(0, cacheSize);
    if (0 == cacheData) {
        textLog->appendPlainText(QLatin1String("Unable to map file to memory"));
        return;
    }

    const GtkIconCacheHeader *const header =
        reinterpret_cast<const GtkIconCacheHeader *>(cacheData);

    textLog->appendPlainText(
            QString::fromLatin1("File Size: %1").arg(cacheFile.size()));

    textLog->appendPlainText(
            QString::fromLatin1("Version %1.%2, Table Offset: %3, List Offset: %4")
                .arg(header->majorVersion())
                .arg(header->minorVersion())
                .arg(header->hashTableOffset())
                .arg(header->dirListOffset()));

    textLog->appendPlainText(
            QString::fromLatin1("Hash Table Size: %1").arg(header->hashTableSize())
            );

    // Hash Table cross-check
    const GtkIconCacheHashTable *hashTable =
        reinterpret_cast<const GtkIconCacheHashTable *>(
                offsetOf(cacheData, cacheData, cacheSize, header->hashTableOffset()));

    if (0 == hashTable) {
        textLog->appendPlainText(QLatin1String("Unable to decode hash table"));
    }
    else {
        textLog->appendPlainText(
                QString::fromLatin1("Hash Table Size (should be same): %1")
                .arg(hashTable->hashTableSize())
                );
    }

    QStringList labels = {
        QLatin1String("ID"),
        QLatin1String("Bucket Offset"),
        QLatin1String("Bucket Name Offset"),
        QLatin1String("Bucket Name"),
        QLatin1String("Image List Count")
    };

    auto model = new QStandardItemModel(this);
    model->setHorizontalHeaderLabels(labels);
    m_ui->m_treeView->setModel(model);

    auto bucketOffsets = hashTable->bucketOffsets();
    auto rootItem = model->invisibleRootItem();
    int i = 0;

    for (qint32 offset : bucketOffsets) {
        auto bucketId = new QStandardItem(QString::number(i++));
        auto bucketOffset = new QStandardItem(QString::number(offset));
        auto bucketNameOffset = new QStandardItem(QLatin1String("No Current Bucket"));
        auto bucketName = new QStandardItem(QLatin1String("None"));
        auto imageListCountText = new QStandardItem(QLatin1String("None"));

        const GtkIconCacheHashBucket *currentBucket = nullptr;

        if (offset != -1) {
            currentBucket =
                reinterpret_cast<const GtkIconCacheHashBucket *>(
                    offsetOf(cacheData, cacheData, cacheSize, offset));
        }

        if (currentBucket) {
            const char *bucketNodeName = reinterpret_cast<const char *>(
                offsetOf(cacheData,
                    cacheData, cacheSize, currentBucket->nodeNameOffset()));

            if (bucketNodeName) {
                bucketNameOffset->setText(QString::number(
                        currentBucket->nodeNameOffset()));
                bucketName->setText(bucketNodeName);
            }

            const qint32 *imageListCount = reinterpret_cast<const qint32 *>(
                    offsetOf(cacheData, cacheData, cacheSize,
                        currentBucket->imageListOffset()));

            if (imageListCountText) {
                imageListCountText->setText(
                        QString::number(
                            qFromBigEndian(*imageListCount)));
            }
        }

        QList<QStandardItem *> itemData = {
            bucketId, bucketOffset, bucketNameOffset, bucketName, imageListCountText
        };
        rootItem->appendRow(itemData);
    }
}

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    GtkIconCacheLoaderMainWindow *w = new GtkIconCacheLoaderMainWindow(0);

    w->show();

    app.connect(&app, SIGNAL(lastWindowClosed()), SLOT(quit()));
    return app.exec();
}
